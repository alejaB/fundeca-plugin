<?php 

/*
Plugin Name: Fundeca
Plugin URI:
Description: Plugin para fundeca
Version: 1.0
Author: Softlond
Author URI:
License:GPL2
License URI: https://www.gnu.org/licenses/gpl-2
Text Domain: fundeca
*/

if(!defined('ABSPATH')) exit; 


require_once plugin_dir_path( __FILE__ ) . 'view/formInscripciones.php';

require_once plugin_dir_path( __FILE__ ) . 'view/gestionInscripciones.php';



